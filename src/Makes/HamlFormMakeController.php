<?php

namespace LaravelScaffold\Makes;

use Illuminate\Support\Arr;
use Laralib\L5scaffold\Makes\MakeController;
use Laralib\L5scaffold\Migrations\SchemaParser;
use LaravelScaffold\Commands\ScaffoldMakeCommand;
use LaravelScaffold\Migrations\SyntaxBuilder;

/**
 * Class HamlFormMakeController
 *
 * @package LaravelScaffold\Makes
 */
class HamlFormMakeController extends MakeController
{
    /** @var ScaffoldMakeCommand */
    protected $scaffoldCommandObj;

    /** {@inheritDoc} */
    protected function compileControllerStub()
    {
        $stub = $this->getStub();

        $this->replaceClassName($stub);
        $this->replaceModelPath($stub);
        $this->replaceModelName($stub);
        $this->replaceSchema($stub, 'controller');
    }

    protected function getStub()
    {
        $stubDir = Arr::get($this->scaffoldCommandObj->getConfig(), 'haml.controllerStubDir');

        if ($stubDir && is_dir($stubDir)) {
            $stubFile = $stubDir.DIRECTORY_SEPARATOR.'controller.stub';

            if (file_exists($stubFile)) {
                return $this->files->get($stubFile);
            }
        }

        throw new \InvalidArgumentException('controller.stub not found');
    }

    protected function replaceModelPath(&$stub)
    {
        $modelName = $this->getAppNamespace().$this->scaffoldCommandObj->getObjName('Name');
        $stub      = str_replace('{{model_path}}', $modelName, $stub);

        return $this;
    }

    protected function replaceModelName(&$stub)
    {
        $modelNameUC = $this->scaffoldCommandObj->getObjName('Name');
        $modelName   = $this->scaffoldCommandObj->getObjName('name');
        $modelNames  = $this->scaffoldCommandObj->getObjName('names');
        $prefix      = $this->scaffoldCommandObj->option('prefix');

        $stub = str_replace('{{model_name_class}}', $modelNameUC, $stub);
        $stub = str_replace('{{model_name_var_sgl}}', $modelName, $stub);
        $stub = str_replace('{{model_name_var}}', $modelNames, $stub);

        if ($prefix != null) {
            $stub = str_replace('{{prefix}}', $prefix.'.', $stub);
        } else {
            $stub = str_replace('{{prefix}}', '', $stub);
        }

        return $this;
    }

    /** {@inheritDoc} */
    protected function replaceSchema(&$stub, $type = 'migration')
    {
        if ($schema = $this->scaffoldCommandObj->option('schema')) {
            $schema = (new SchemaParser)->parse($schema);
        }

        // Create controllers fields
        $schema     = (new SyntaxBuilder)->create($schema, $this->scaffoldCommandObj->getMeta(), 'controller');
        $stub       = str_replace('{{model_fields}}', $schema, $stub);
        $formFields = '';

        foreach ($schema as $field => $meta) {
            $formFields .= "\n";
            $formFields .= "\$this->form->addText('{$field}')";
        }

        $stub = str_replace('{{form_fields}}', $formFields, $stub);

        return $this;
    }
}