<?php

namespace LaravelScaffold\Makes;

use Illuminate\Filesystem\Filesystem;
use Laralib\L5scaffold\Commands\ScaffoldMakeCommand;
use Laralib\L5scaffold\Makes\MakeLayout as BaseMakeLayout;

/**
 * Class MakeLayout
 *
 * @package LaravelScaffold\Makes
 */
class MakeLayout extends BaseMakeLayout
{
    /** @var ScaffoldMakeCommand */
    protected $scaffoldCommand;

    public function __construct(ScaffoldMakeCommand $scaffoldCommand, Filesystem $files)
    {
        $this->scaffoldCommand = $scaffoldCommand;

        parent::__construct($scaffoldCommand, $files);
    }

    /** {@inheritDoc} */
    protected function putViewLayout($name, $stub, $file)
    {
        $file     = str_replace('.blade.php', '.haml', $file);
        $stub     = str_replace('stubs/html_assets/', '', $stub);
        $pathFile = $this->getPathResource().$file;
        $pathStub = resource_path('stubs/views/'.$stub);

        if (!file_exists($pathStub)) {
            parent::putViewLayout($name, $stub, $file);
        }

        if (!$this->files->exists($pathFile)) {
            $this->files->put($pathFile, $this->files->get($pathStub));

            $this->scaffoldCommand->info("$name created successfully.");
        } else {
            $this->scaffoldCommand->comment("Skip $name, because already exists.");
        }
    }
}