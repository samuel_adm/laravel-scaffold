<?php

namespace LaravelScaffold\Makes;

use LaravelScaffold\Migrations\SyntaxBuilder;
use Laralib\L5scaffold\Makes\MakeView as BaseMakeView;

/**
 * Class MakeView
 *
 * @package LaravelScaffold\Makes
 */
class MakeView extends BaseMakeView
{
    /** {@inheritDoc} */
    protected function getPath($file_name, $path = 'controller')
    {
        if ($path == "view-index") {
            return './resources/views/'.$file_name.'/index.haml';
        } elseif ($path == "view-edit") {
            return './resources/views/'.$file_name.'/edit.haml';
        } elseif ($path == "view-show") {
            return './resources/views/'.$file_name.'/show.haml';
        } elseif ($path == "view-create") {
            return './resources/views/'.$file_name.'/create.haml';
        }
    }

    /** {@inheritDoc} */
    protected function compileViewStub($nameView)
    {
        $stub = resource_path('stubs/views/'.$nameView.'.stub');

        if (!file_exists($stub)) {
            return parent::compileViewStub($nameView);
        }

        $stub = $this->files->get($stub);

        if ($nameView == 'show') {
            // show.haml
            $this->replaceName($stub)
                ->replaceSchemaShow($stub);
        } elseif ($nameView == 'edit') {
            // edit.haml
            $this->replaceName($stub)
                ->replaceSchemaEdit($stub);
        } elseif ($nameView == 'create') {
            // edit.haml
            $this->replaceName($stub)
                ->replaceSchemaCreate($stub);
        } else {
            // index.haml
            $this->replaceName($stub)
                ->replaceSchemaIndex($stub);
        }

        return $stub;
    }

    /**
     * Replace the schema for the index.stub.
     *
     * @param  string $stub
     *
     * @return $this
     */
    protected function replaceSchemaIndex(&$stub)
    {

        // Create view index header fields
        $schema = (new SyntaxBuilder)->create($this->schemaArray, $this->scaffoldCommandObj->getMeta(), 'view-index-header');
        $stub   = str_replace('{{header_fields}}', $schema, $stub);

        // Create view index content fields
        $schema = (new SyntaxBuilder)->create($this->schemaArray, $this->scaffoldCommandObj->getMeta(), 'view-index-content');
        $stub   = str_replace('{{content_fields}}', $schema, $stub);

        return $this;
    }

    /**
     * Replace the schema for the show.stub.
     *
     * @param  string $stub
     *
     * @return $this
     */
    protected function replaceSchemaShow(&$stub)
    {
        // Create view index content fields
        $schema = (new SyntaxBuilder)->create($this->schemaArray, $this->scaffoldCommandObj->getMeta(), 'view-show-content');
        $stub   = str_replace('{{content_fields}}', $schema, $stub);

        return $this;
    }

    /**
     * Replace the schema for the edit.stub.
     *
     * @param  string $stub
     *
     * @return $this
     */
    protected function replaceSchemaEdit(&$stub)
    {
        // Create view index content fields
        $schema = (new SyntaxBuilder)->create($this->schemaArray, $this->scaffoldCommandObj->getMeta(), 'view-edit-content', $this->scaffoldCommandObj->option('form'));
        $stub   = str_replace('{{content_fields}}', $schema, $stub);

        return $this;
    }

    /**
     * Replace the schema for the edit.stub.
     *
     * @param  string $stub
     *
     * @return $this
     */
    protected function replaceSchemaCreate(&$stub)
    {
        // Create view index content fields
        $schema = (new SyntaxBuilder)->create($this->schemaArray, $this->scaffoldCommandObj->getMeta(), 'view-create-content', $this->scaffoldCommandObj->option('form'));
        $stub   = str_replace('{{content_fields}}', $schema, $stub);

        return $this;
    }
}