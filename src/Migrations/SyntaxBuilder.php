<?php

namespace LaravelScaffold\Migrations;

use Laralib\L5scaffold\Migrations\SyntaxBuilder as BaseSyntaxBuilder;

/**
 * Class SyntaxBuilder
 *
 * @package LaravelScaffold\Migrations
 */
class SyntaxBuilder extends BaseSyntaxBuilder
{
    /** {@inheritDoc} */
    public function create($schema, $meta, $type = "migration", $illuminate = false)
    {
        switch ($type) {
            case 'view-index-header':
            case 'view-index-content':
            case 'view-show-content':
            case 'view-edit-content':
            case 'view-create-content':
                return $this->createSchemaForViewMethod($schema, $meta, str_replace('view-', '', $type));
            default:
                return parent::create($schema, $meta, $type, $illuminate);
        }
    }

    protected function buildField($field, $type, $variable, $value = true)
    {
        $column = strtolower($field['name']);
        $title  = ucfirst($field['name']);

        if ($value === true) {
            $value = ''.$variable.'.'.$column;
        } else {
            $value = 'old("'.$column.'")';
        }

        $syntax = [];

        switch ($type) {
            case 'string':
            default:
                $input = 'text';
                break;
            case 'text':
                $input = 'textarea';
                break;
        }

        $syntax[] = '';
        $syntax[] = '.form-group{:class => errors.has('."'".$column."'".') ? \'has-error\' : \'\'}';
        $syntax[] = '  %label{:for => "'.$column.'-field"} '.$title;

        if ($this->illuminate) {
            $syntax[] = '  = Form.'.$input.'("'.$column.'", '.$value.', {"class": "form-control", "id": "'.$column.'-field"})';
        } else {
            $syntax[] = '  '.$this->htmlField($column, $variable, $field, $type);
        }

        $syntax[] = '  - if errors.has("'.$column.'")';
        $syntax[] = '    %span.help-block= errors.first("'.$column.'")';

        return join("\n".str_repeat(' ', 8), $syntax);
    }

    protected function htmlField($column, $variable, $field, $type)
    {
        $value = 'old("'.$column.'")';

        if ($type == 'edit-content') {
            $value = 'old("'.$column.'") is null ? '.$variable.'.'.$column.' : old("'.$column.'")';
        }

        switch ($field['type']) {
            case 'string':
            default:
                $layout = "%input.form-control{:type => \"text\", :id => \"$column-field\", :name => \"$column\", :value => $value}";
                break;
            case 'date':
                $layout = "%input.form-control.date-picker{:type => \"text\", :id => \"$column-field\", :name => \"$column\", :value => $value}";
                break;
            case 'boolean':
                $layout = ".btn-group{'data-toggle' => \"buttons\"}";
                $layout .= "\n"."  %label.btn.btn-primary";
                $layout .= "\n"."    %input{:type => 'radio', :value => 'true', :name => \"$column-field\", :id => \"$column-field\", :autocomplete => 'off'}";
                $layout .= "\n"."    True";
                $layout .= "\n"."  %label.btn.btn-primary";
                $layout .= "\n"."    %input{:type => 'radio', :value => 'false', :name => \"$column-field\", :id => \"$column-field\", :autocomplete => 'off'}";
                $layout .= "\n"."    False";
                break;
            case 'text':
                $layout = "%textarea.form-control{:id => \"$column-field\", :rows => \"3\", :name => \"$column\"}";
                $layout .= "\n"."  = $value";
                break;
        }

        return $layout;
    }

    protected function createSchemaForViewMethod($schema, $meta, $type = 'index-header')
    {
        if (!$schema) {
            return '';
        }

        switch ($type) {
            case 'index-header':
                $callback = function ($field) use ($meta, $type) { return sprintf("%%th %s", strtoupper($field['name'])); };
                break;
            case 'index-content':
                $callback = function ($field) use ($meta, $type) { return sprintf("%%td= %s.%s", $meta['var_name'], strtolower($field['name'])); };
                break;
            case 'show-content':
                $callback = function ($field) use ($meta, $type) {
                    return sprintf(".form-group\n".
                        str_repeat(' ', 10)."%%label{:for => \"%s\"} %s\n".
                        str_repeat(' ', 10)."%%p.form-control-static= %s.%s\n", strtolower($field['name']), strtoupper($field['name']), $meta['var_name'], strtolower($field['name']));
                };
                break;
            case 'edit-content':
                $callback = function ($field) use ($meta, $type) { return $this->buildField($field, $type, $meta['var_name'], true); };
                break;
            case 'create-content':
                $callback = function ($field) use ($meta, $type) { return $this->buildField($field, $type, $meta['var_name'], false); };
                break;
        }

        if (!isset($callback)) {
            return '';
        }

        $fields = array_map($callback, $schema);

        if ($type == 'index-header') {
            return implode("\n".str_repeat(' ', 14), $fields);
        } elseif ($type === 'index-content') {
            return implode("\n".str_repeat(' ', 18), $fields);
        } else {
            return implode(str_repeat(' ', 8), $fields);
        }
    }
}