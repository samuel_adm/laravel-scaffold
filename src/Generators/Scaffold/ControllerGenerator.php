<?php

namespace InfyOm\Generator\Generators\Scaffold;

use InfyOm\Generator\Common\CommandData;
use InfyOm\Generator\Generators\BaseGenerator;
use InfyOm\Generator\Utils\FileUtil;
use InfyOm\Generator\Utils\GeneratorFieldsInputUtil;

/**
 * Class ControllerGenerator
 *
 * @package InfyOm\Generator\Generators\Scaffold
 */
class ControllerGenerator extends BaseGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;

    /** @var string */
    private $templateType;

    /** @var string */
    private $fileName;

    public function __construct(CommandData $commandData)
    {
        $this->commandData  = $commandData;
        $this->path         = $commandData->config->pathController;
        $this->templateType = config('infyom.laravel_generator.templates', 'core-templates');
        $this->fileName     = $this->commandData->modelName.'Controller.php';
    }

    public function generate()
    {
        if ($this->commandData->getAddOn('datatables')) {
            $templateData = get_template('scaffold.controller.datatable_controller', 'laravel-generator');

            $this->generateDataTable();
        } else {
            $templateData = get_template('advancedForm.controller.controller', 'laravel-generator');
            $paginate     = $this->commandData->getOption('paginate');

            if ($paginate) {
                $templateData = str_replace('$RENDER_TYPE$', 'paginate('.$paginate.')', $templateData);
            } else {
                $templateData = str_replace('$RENDER_TYPE$', 'all()', $templateData);
            }
        }

        $vars = $this->commandData->dynamicVars;

        if ($this->commandData->getAddOn('advancedForm')) {
            $formFields = $this->generateAdvancedFormFields();

            $vars['$FORM_FIELDS$'] = implode(infy_nl_tab(1, 2), $formFields);
        }  
        $relations = [];
        foreach($this->commandData->relations as $relation) {
            if ($relation->type == '1tm') {
                $relations[] = $relation->props['functionName'];
            }
        }
        $vars['$WITH_RELATION$'] = '';
        if ($relations) {
            $vars['$WITH_RELATION$'] = 'with(["'.implode('","', $relations).'"])->';
        }

        $templateData = fill_template($vars, $templateData);

        FileUtil::createFile($this->path, $this->fileName, $templateData);

        $this->commandData->commandComment("\nController created: ");
        $this->commandData->commandInfo($this->fileName);
    }

    protected function generateAdvancedFormFields()
    {
        $formFields     = [];
        $accessorString = fill_template($this->commandData->dynamicVars, "\$this->\$MODEL_NAME_CAMEL\$Form");

        $relatedFields = [];
        foreach($this->commandData->relations as $relation) {
            // proccess only belongsTo relation
            if ($relation->type == 'mt1') {
                $relatedFields[$relation->props['localField']] = $relation->props;
            }
        }

        $selectOptionModels = [];
        foreach ($this->commandData->fields as $field) {
            $validationString = '';
            $validationArr    = [];
            $validationArrRaw = is_array($field->validations)
                ? $field->validations
                : array_map('trim', explode(',', $field->validations));

            foreach ($validationArrRaw as $validationItem) {
                if ($validationItem) {
                    $params = [];

                    preg_match_all('/\[([^]]+)+\]/', $validationItem, $params);

                    $params = $params && isset($params[1]) ? $params[1] : [];
                    $params = array_map(function ($param) {
                        return is_numeric($param)
                            ? $param
                            : "'{$param}'";
                    }, $params);

                    $validationArr[] = [
                        'rule'   => explode('[', $validationItem)[0],
                        'params' => $params
                    ];
                }
            }

            foreach ($validationArr as $validationRule) {
                $methodParams      = ["'{$validationRule['rule']}'"] + $validationRule['params'];
                $validationString .= infy_nl_tab(1, 3)."->addRule(".implode(', ', $methodParams).")";
            }

            if (!$field->htmlType) {
                $field->htmlType = 'text';
            }

            $option = '';
            if (isset($relatedFields[$field->name])) {
                $relation = $relatedFields[$field->name];
                $type = 'select';
                $option = '$'.str_plural(camel_case($relation['modelName'])).'_options';
                if (!isset($selectOptionModels[$relation['modelName']])) {
                    $selectOptionModels[$relation['modelName']] = ['var' => $option, 'value' => $relation['foreignField'], 'text' => $relation['secondField']];
                }
            } else {
                list($type) = explode(',',$field->htmlType);
            }

            switch ($type) {
                case 'text':
                case 'textarea':
                case 'date':
                case 'file':
                case 'email':
                case 'password':
                case 'number':
                    $fieldTemplate = strtr(":accessor->add:fieldType(':fieldName')", [
                        ':accessor'  => $accessorString,
                        ':fieldType' => ucfirst($field->htmlType),
                        ':fieldName' => $field->name
                    ]);
                    $fieldTemplate = $fieldTemplate.$validationString.';';
                    break;
                case 'select':
                case 'enum':
                    if (!$option) {
                        $inputsArr     = explode(',', $field['htmlTypeInputs']);
                    }
                    $fieldTemplate = strtr(":accessor->addSelect(':fieldName', :options)", [
                        ':accessor'  => $accessorString,
                        ':options'   => $option ? $option : GeneratorFieldsInputUtil::prepareKeyValueArrayStr($inputsArr),
                        ':fieldName' => $field->name
                    ]);
                    
                    $fieldTemplate = $fieldTemplate.$validationString.';';
                    break;
                case 'radio':
                    $fieldTemplate = '';
                    $radioLabels   = GeneratorFieldsInputUtil::prepareKeyValueArrFromLabelValueStr($field->htmlValues);

                    foreach ($radioLabels as $label => $value) {
                        $fieldTemplate = $fieldTemplate.(strtr(":accessor->addRadio(':fieldName[]', ':value')", [
                                    ':accessor'  => $accessorString,
                                    ':fieldName' => $field->name,
                                    ':value'     => $value
                                ]).$validationString.';');
                    }
                    break;
                case 'bool-checkbox':
                    $checkboxValue = $value = $field['htmlTypeInputs'];

                    if ($field['fieldType'] === 'boolean') {
                        if ($checkboxValue === 'checked') {
                            $checkboxValue = '1, true';
                        } elseif ($checkboxValue === 'unchecked') {
                            $checkboxValue = '0';
                        }
                    }

                    $fieldTemplate = strtr(":accessor->addCheckbox(':fieldName', ':fieldValue')", [
                        ':accessor'   => $accessorString,
                        ':fieldName'  => $field->name,
                        ':fieldValue' => $checkboxValue
                    ]);
                    $fieldTemplate = $fieldTemplate.$validationString.';';
                    break;
                //case 'toggle-switch':
                //    $fieldTemplate = get_template('advancedForm.fields.toggle-switch', 'laravel-generator');
                //    $checkboxValue = $value = $field['htmlTypeInputs'];
                //    if ($field['fieldType'] === 'boolean') {
                //        $checkboxValue = "[ 'On' => '1' , 'Off' => '0']";
                //    }
                //    $fieldTemplate = str_replace('$CHECKBOX_VALUE$', $checkboxValue, $fieldTemplate);
                //    //$fieldTemplate = str_replace('$VALUE$', $value, $fieldTemplate);
                //    break;
                case 'checkbox':
                    $tmp = explode(',', $field->htmlType);
                    $checkboxValue = $tmp[2];

                    $fieldTemplate = strtr(":accessor->addCheckbox(':fieldName', ':fieldValue')", [
                        ':accessor'   => $accessorString,
                        ':fieldName'  => $field->name,
                        ':fieldValue' => $checkboxValue
                    ]);
                    $fieldTemplate = $fieldTemplate.$validationString.';';
                    break;
                case 'boolean':
                    //$fieldTemplate = get_template('advancedForm.fields.boolean', 'laravel-generator');
                    //$checkboxValue = $value = $field['htmlTypeInputs'];
                    //if ($field['fieldType'] == 'boolean') {
                    //    $checkboxValue = true;
                    //}
                    //$fieldTemplate = str_replace('$CHECKBOX_VALUE$', $checkboxValue, $fieldTemplate);
                    //// $fieldTemplate = str_replace('$VALUE$', $value, $fieldTemplate);
                    //break;
                default:
                    $fieldTemplate = '';
                    break;
            }

            if ($fieldTemplate) {
                $formFields[] = $fieldTemplate;
            }
        }
        
        $optionVars = [];
        if (count($selectOptionModels)) {
            foreach ($selectOptionModels as $modelName => $option) {
                $optionVars[] = strtr(':varname = :model::pluck(":optionlabel", ":optionvalue")->prepend("")->toArray();', [
                    ':varname' => $option['var'],
                    ':optionlabel' => $option['text'],
                    ':optionvalue' => $option['value'],
                    ':model' => '\\'.$this->commandData->dynamicVars['$NAMESPACE_MODEL$'].'\\'.$modelName
                ]);
            }
        }


        $formFields = array_merge($optionVars, $formFields);

        return $formFields;
    }

    protected function generateDataTable()
    {
        $templateData        = get_template('scaffold.datatable', 'laravel-generator');
        $templateData        = fill_template($this->commandData->dynamicVars, $templateData);
        $headerFieldTemplate = get_template('scaffold.views.datatable_column', $this->templateType);

        $headerFields = [];

        foreach ($this->commandData->fields as $field) {
            if (!$field->inIndex) {
                continue;
            }

            $headerFields[] = $fieldTemplate = fill_template_with_field_data(
                $this->commandData->dynamicVars,
                $this->commandData->fieldNamesMapping,
                $headerFieldTemplate,
                $field
            );
        }

        $path         = $this->commandData->config->pathDataTables;
        $fileName     = $this->commandData->modelName.'DataTable.php';
        $fields       = implode(','.infy_nl_tab(1, 3), $headerFields);
        $templateData = str_replace('$DATATABLE_COLUMNS$', $fields, $templateData);

        FileUtil::createFile($path, $fileName, $templateData);

        $this->commandData->commandComment("\nDataTable created: ");
        $this->commandData->commandInfo($fileName);
    }

    public function rollback()
    {
        if ($this->rollbackFile($this->path, $this->fileName)) {
            $this->commandData->commandComment('Controller file deleted: '.$this->fileName);
        }

        if ($this->commandData->getAddOn('datatables')) {
            if ($this->rollbackFile($this->commandData->config->pathDataTables, $this->commandData->modelName.'DataTable.php')) {
                $this->commandData->commandComment('DataTable file deleted: '.$this->fileName);
            }
        }
    }
}
