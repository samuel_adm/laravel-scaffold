<?php

namespace LaravelScaffold\Commands;

use Illuminate\Console\Application;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Composer;
use Laralib\L5scaffold\Makes\MakeMigration;
use LaravelScaffold\Makes\MakeLayout;
use LaravelScaffold\Makes\MakeView;
use Laralib\L5scaffold\Makes\MakeController;
use Laralib\L5scaffold\Makes\MakeSeed;
use Laralib\L5scaffold\Commands\ScaffoldMakeCommand as BaseMakeCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class ScaffoldMakeCommand
 *
 * @package LaravelScaffold\Commands
 */
class ScaffoldMakeCommand extends BaseMakeCommand
{
    /** @var \Illuminate\Contracts\Foundation\Application */
    protected $app;
    /** @var array */
    protected $config;

    public function __construct(Filesystem $files, Composer $composer, Application $app)
    {
        parent::__construct($files, $composer);

        $this->app = $app->getLaravel();
    }

    /** {@inheritDoc} */
    public function fire()
    {
        $availableTypes = array_keys($this->app['config']['scaffold.types']);

        if (!in_array($this->option('type'), $availableTypes)) {
            $this->error('Unknown type');

            exit;
        }

        $this->config = $this->app['config']['scaffold.types'][$this->option('type')];

        $this->info('Configuring '.$this->getObjName("Name").'...');

        $this->meta['action']   = 'create';
        $this->meta['var_name'] = $this->getObjName("name");
        $this->meta['table']    = $this->getObjName("names");

        $this->makeMigration();
        $this->makeSeed();
        $this->makeModel();
        $this->makeController();
        $this->makeViewLayout();
        $this->makeViews();
    }

    /** {@inheritDoc} */
    protected function getOptions()
    {
        $options   = parent::getOptions();
        $options[] = ['type', 't', InputOption::VALUE_OPTIONAL, 'Type to use', 'default'];

        return $options;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the model. (Ex: Post)'],
        ];
    }

    /** {@inheritDoc} */
    protected function makeMigration()
    {
        return $this->config['makeMigration']
            ? new $this->config['makeMigration']($this, $this->files)
            : new MakeMigration($this, $this->files);
    }

    /**
     * Generate a Seed
     */
    protected function makeSeed()
    {
        return $this->config['makeSeed']
            ? new $this->config['makeSeed']($this, $this->files)
            : new MakeSeed($this, $this->files);
    }

    /**
     * Make a Controller with default actions
     */
    protected function makeController()
    {
        return $this->config['makeController']
            ? new $this->config['makeController']($this, $this->files)
            : new MakeController($this, $this->files);
    }

    /**
     * Setup views and assets
     *
     */
    protected function makeViews()
    {
        foreach (['index', 'create', 'show', 'edit'] as $view) {
            return $this->config['makeView']
                ? new $this->config['makeView']($this, $this->files, $view)
                : new MakeView($this, $this->files, $view);
        }

        $this->info('Views created successfully.');
        $this->info('Dump-autoload...');
        $this->info('Route::resource("'.$this->getObjName("names").'","'.$this->getObjName("Name").'Controller"); // Add this line in routes.php');
    }

    /**
     * Make a layout.blade.php with bootstrap
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function makeViewLayout()
    {
        return $this->config['makeLayout']
            ? new $this->config['makeLayout']($this, $this->files)
            : new MakeLayout($this, $this->files);
    }

    /**
     * Get access to $meta array
     *
     * @return array
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Generate names
     *
     * @param string $config
     *
     * @return mixed
     * @throws \Exception
     */
    public function getObjName($config = 'Name')
    {
        $names     = [];
        $args_name = $this->argument('name');

        // Name[0] = Tweet
        $names['Name'] = str_singular(ucfirst($args_name));
        // Name[1] = Tweets
        $names['Names'] = str_plural(ucfirst($args_name));
        // Name[2] = tweets
        $names['names'] = str_plural(strtolower(preg_replace('/(?<!^)([A-Z])/', '_$1', $args_name)));
        // Name[3] = tweet
        $names['name'] = str_singular(strtolower(preg_replace('/(?<!^)([A-Z])/', '_$1', $args_name)));

        if (!isset($names[$config])) {
            throw new \Exception("Position name is not found");
        };

        return $names[$config];
    }

    public function getConfig()
    {
        return $this->config;
    }
}